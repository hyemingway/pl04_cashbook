import java.io.*;
import java.util.ArrayList;
import java.util.*;

/**
 * Created by DELL on 2015-05-20.
 */
public class FileCTRL {
    //���� ���� class
    private ArrayList<CashModel> list = new ArrayList<CashModel>();
    ;  //���� �ֽ� CashBook �׸���� ���⿡ �����Ѵ�.
    private PrintWriter pw = null;

    public FileCTRL() {
        try {
            pw = new PrintWriter(new FileWriter("moneybook.txt", true));
//            pw.print("test");
        } catch (IOException e) {
            e.printStackTrace();
        }
        LoadFromFile();
    }

    public synchronized void LoadFromFile() {
        //list �ʱ�ȭ
        list.clear();
        //list�� text File Loading

        BufferedReader br = null;
        try {
            br = new BufferedReader(new FileReader("moneybook.txt"));
            while (true) {
                String line = null;
                try {
                    line = br.readLine();   //�� �� �о��
                    if (line == null) break;    //null �̸� break;
                    CashModel temp = Tokenize(line);
                    System.err.println(temp.toString());
                    list.add(temp);

                } catch (IOException e) {
                    e.printStackTrace();
                }
            }//while��
//            debugPrint();

            try {
                br.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    private CashModel Tokenize(String line) {
        CashModel cm = null;
        String[] tokens = line.split("[|]");
        if(tokens.length == 4){
            cm = new CashModel(tokens[0], tokens[1], tokens[2], Integer.parseInt(tokens[3]));
        }else{
            System.err.println("Load Fail");
        }
        return cm;
    }

    public synchronized void SaveToFile(/*ArrayList<CashModel> cashList*/){
        PrintWriter pw = null;
        try {
            pw = new PrintWriter("moneybook.txt");
            for (CashModel cm : list) {
                String data = cm.toString();    //type|date|content|money
                pw.println(data);   // print line
            }
            pw.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        LoadFromFile();
    }

    public ArrayList<CashModel> getList(){
        return this.list;
    }

    public synchronized void setList(ArrayList<CashModel> ls){
        this.list = ls;
    }

    public synchronized void DeleteModelsFromData(ArrayList<CashModel> dellist) {

        Iterator iter = list.iterator();
        while(iter.hasNext()) {
            CashModel obj = (CashModel) iter.next();
            for(CashModel d : dellist){
                boolean check = obj.equals(d);
                if(obj.toString().equals(d.toString())){
                    iter.remove();
                }
            }
        }
//        Iterator it = list.iterator();
//        while(it.hasNext()){
//            CashModel c = (CashModel) it.next();
//            for(CashModel d: dellist){
//                if (c.toString().equals(d.toString())) {
//                    it.remove();
//                    break;
//                }
//            }
//        }
//        debugPrint();
    }

    public void debugPrint(){
        for(CashModel c : list){
            System.err.println(c.toString());
        }

    }

}
