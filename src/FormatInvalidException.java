/**
 * Created by DELL on 2015-05-18.
 */
public class FormatInvalidException extends Exception{
    //형식을 벗어나면 메세지 출력
    //금액 입력란에 숫자 이외의 기호 또는 문자가 입력되는 경우
    public FormatInvalidException(){
        super("It does not fit the format :)");
//        System.err.println();
    }

}
