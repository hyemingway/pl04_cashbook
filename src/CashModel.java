/**
 * Created by DELL on 2015-05-17.
 */
public class CashModel {
    //가계부 항목 data class
    //모든 항목은 money 제외하고 다 String으로 받는다
    //date의 경우, String으로 받은 date를 년 월 일로 파싱, 각각 다시 저장한다
    private String type;
    private String date;
    private String content;
    private int money;
    private int year = 0;
    private int month = 0;
    private int day = 0;

    //기본 생성자
    public CashModel(String type, String date, String content, int money) {
        this.type = type;
        this.date = date;
        this.content = content;
        this.money = money;

        DateTokenize(this.date);


    }

    public String getType() {
        return this.type;
    }

    public String getDate() {
        return this.date;
    }

    public String getContent() {
        return this.content;
    }

    public int getYear() {
        return this.year;
    }

    public int getMonth() {
        return this.month;
    }

    public int getDay() {
        return this.day;
    }

    public void DateTokenize(String date) {
        String[] token = date.split("-");
        if (token.length != 3) {
            try {
                throw new FormatInvalidException();
            } catch (FormatInvalidException e) {
                e.printStackTrace();
            }
        }
        this.year = Integer.parseInt(token[0]);
        this.month = Integer.parseInt(token[1]);
        this.day = Integer.parseInt(token[2]);


    }

    public String toString() {
        //저장. 출력용 dump
        String str = this.type + "|" + this.date + "|" + this.content + "|" + this.money;
        return str;
    }
}
