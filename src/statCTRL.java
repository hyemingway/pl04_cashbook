import javax.swing.*;
import java.lang.reflect.Array;
import java.util.ArrayList;

/**
 * Created by DELL on 2015-05-18.
 */
public class StatCTRL {
    private String checking;
    private ArrayList<CashModel> statList = new ArrayList<>();
    private FileCTRL mFileCTRL = new FileCTRL();
    private ArrayList<CashModel> baseList = mFileCTRL.getList();

    public StatCTRL(String check) {
        this.checking = check;
    }

    public void StatYear(String year) {
        statList.clear();
        try {
            int y = Integer.parseInt(year);
            for (CashModel cm : baseList) {
                if (cm.getYear() == y) {
                    statList.add(cm);
                }
            }
        } catch (Exception e) {
            try {
                throw new FormatInvalidException();
            } catch (FormatInvalidException e1) {
                JOptionPane.showMessageDialog(null, e1.getMessage());
                e1.printStackTrace();
            }

        }
    }

    public void StatMonth(String month) {
        statList.clear();
        try {
            int m = Integer.parseInt(month);
            for (CashModel cm : baseList) {
                if (cm.getMonth() == m) {
                    statList.add(cm);
                }
            }
        } catch (Exception e) {
            try {
                throw new FormatInvalidException();
            } catch (FormatInvalidException e1) {
                JOptionPane.showMessageDialog(null, e1.getMessage());
                e1.printStackTrace();
            }

        }


    }

    public void StatWeek(String date) {
        statList.clear();
        JOptionPane.showMessageDialog(null, "This function is not implemented :<");
    }

    public ArrayList<CashModel> getStatList() {
        return this.statList;
    }

    public String getResult() {
        if (!statList.isEmpty()) {
            String res = "";
            for (CashModel cm : baseList) {
                res += cm.toString();
                res += "\r\n";
            }
            return res;
        } else {
            return "no stats";
        }
    }

}
