/**
 * Created by DELL on 2015-05-18.
 */
public class CharacterExcessException extends Exception{
    public CharacterExcessException(){
        super("Please enter less than 20 characters :)");
    }
}
