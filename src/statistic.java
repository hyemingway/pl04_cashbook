import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created by DELL on 2015-05-18.
 */
public class statistic extends JPanel{
    private JRadioButton outgoingRadioButton;
    private JRadioButton incomingRadioButton;
    private JButton weeklyButton;
    private JButton monthlyButton;
    private JButton yearlyButton;
    private JTextArea stat_text_area;
    private JPanel panel3;
    private JTextField text_today;

    private String checkType = "";

    private StatCTRL mStatCTRL;

    public statistic(){
        outgoingRadioButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                checkType = "OUT";
                mStatCTRL = new StatCTRL(checkType);
            }
        });
        incomingRadioButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                checkType = "IN";
                mStatCTRL = new StatCTRL(checkType);
            }
        });
        weeklyButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                stat_text_area.setText("");
                String [] tokens = text_today.getText().split("-");
                mStatCTRL.StatWeek(tokens[2]);
                String result = mStatCTRL.getResult();
                stat_text_area.setText(result);
            }
        });

        monthlyButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                stat_text_area.setText("");
                String [] tokens = text_today.getText().split("-");
                mStatCTRL.StatMonth(tokens[1]);
                String result = mStatCTRL.getResult();
                stat_text_area.setText(result);

            }
        });

        yearlyButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                stat_text_area.setText("");
                String [] tokens = text_today.getText().split("-");
                mStatCTRL.StatYear(tokens[0]);
                String result = mStatCTRL.getResult();
                stat_text_area.setText(result);

            }
        });
    }

    public void Print(){

    }


}
