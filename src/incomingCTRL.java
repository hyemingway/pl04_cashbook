import javax.swing.*;
import java.util.ArrayList;
import java.util.Objects;

/**
 * Created by DELL on 2015-05-18.
 */
public class IncomingCTRL {
    private FileCTRL mFileCTRL = new FileCTRL();
    private String result;

    public IncomingCTRL() {
        result = null;
    }


    public void Register(String date, String content, int money) {

        CashModel cm = new CashModel("IN", date, content, money);
        ArrayList<CashModel> list = mFileCTRL.getList();

        try {
            boolean isThere = false;
            for(CashModel c : list){
                if(c.toString().equals(cm.toString())){
                    isThere = true;
                    break;
                }
            }
//                    = list.contains(cm);
            if (isThere) {
                throw new DuplicateItemException();
            } else {
                list.add(cm);
                mFileCTRL.setList(list);
                mFileCTRL.SaveToFile();

                JOptionPane.showMessageDialog(null, "REGISTRATION COMPLETE");
            }

        } catch (DuplicateItemException e) {
            JOptionPane.showMessageDialog(null, e.getMessage());
            e.printStackTrace();
        }

    }

    public ArrayList<CashModel> Search(String date) {
        mFileCTRL.LoadFromFile();
//        System.err.println("DEBUG : " + date);
        ArrayList<CashModel> cashs = new ArrayList<>();
        ArrayList<CashModel> list = mFileCTRL.getList();
        for (CashModel cm : list) {
            if (Objects.equals(cm.getDate(), date) && Objects.equals(cm.getType(), "IN")) {
                cashs.add(cm);
            }
        }
        if (cashs.isEmpty()) return null;
        return cashs;
    }

}
