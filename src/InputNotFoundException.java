/**
 * Created by DELL on 2015-05-18.
 */
public class InputNotFoundException extends Exception {
    public InputNotFoundException() {
        super("No information has been entered :)");
    }
}
