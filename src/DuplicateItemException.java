/**
 * Created by DELL on 2015-05-18.
 */
public class DuplicateItemException extends Exception {
    public DuplicateItemException(){
        super("The duplicate item already exists. Please Enter another item case :)");
    }
}
