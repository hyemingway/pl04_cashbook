import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

/**
 * Created by DELL on 2015-05-17.
 */

public class incoming extends JPanel{
    private JPanel panel1;
    private JButton SEARCHButton;
    private JButton DELETEButton;
    private JTextField text_money;
    private JTextField text_content;
    private JButton REGISTERButton;
    private JTextArea textArea1;
    private JTextField text_date;  //datefield

    private IncomingCTRL mIncomingCTRL;
    private FileCTRL mFileCTRL;
    private ArrayList<CashModel> searchResult = new ArrayList<>();

    public incoming(){
        mIncomingCTRL = new IncomingCTRL();
        mFileCTRL = new FileCTRL();

        //add listener
        SEARCHButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Thread th = new Thread(){
                  @Override
                    public void run(){
                      try {
                          Thread.sleep(1000);
                      } catch (InterruptedException e1) {
                          e1.printStackTrace();
                      }
                      try {
                          searchResult.clear();
//                          System.err.println("DEBUG : " + searchResult.toString());
                          String d = text_date.getText();
                          ArrayList<CashModel> res = mIncomingCTRL.Search(d);
                          searchResult = res;
//                          System.err.println("DEBUG" + res);
                          if (res != null) {
                              PrintOut(res);
                          } else {
                              throw new SearchNotFoundException();
                          }
                      }catch(SearchNotFoundException e){
                          JOptionPane.showMessageDialog(null, e.getMessage());
                      }
                  }
                };

                th.start();
            }
        });

        DELETEButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Thread th = new Thread(){
                    @Override
                    public void run() {
                        try {
//                        ArrayList<CashModel> del = mIncomingCTRL.Search(text_date.getText());
                            if (searchResult != null) {
                                mFileCTRL.DeleteModelsFromData(searchResult);
//                            mFileCTRL.DeleteModelsFromData(del);
                                mFileCTRL.SaveToFile();
                                mFileCTRL.LoadFromFile();
                                JOptionPane.showMessageDialog(null, "DELETE SUCCESS");

                                textArea1.setText("");
                            } else {
                                throw new SearchNotFoundException();
                            }
                        } catch (SearchNotFoundException e) {
                            JOptionPane.showMessageDialog(null, e.getMessage());
                        } catch (ArrayIndexOutOfBoundsException e) {
                            System.err.println("DEBUG : " + e.getStackTrace());
                        }
                    }
                };
                th.start();
            }
        });

        REGISTERButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Thread th = new Thread(){
                    @Override
                    public void run(){
                        try {
                            String date = text_date.getText();
                            String content = text_content.getText();
                            if(content.length() > 20){
                                throw new CharacterExcessException();
                            }
                            if(date.isEmpty() || content.isEmpty() || text_money.getText().isEmpty()){
                                throw new InputNotFoundException();
                            }

                            int money = Integer.parseInt(text_money.getText());

                            mIncomingCTRL.Register(date, content, money);

                        } catch (CharacterExcessException | InputNotFoundException e) {
                            JOptionPane.showMessageDialog(null, e.getMessage());
                        } catch (NumberFormatException e) {
                            try {
                                throw new FormatInvalidException();
                            } catch (FormatInvalidException e1) {
                                JOptionPane.showMessageDialog(null, e1.getMessage());
                                e1.printStackTrace();
                            }
                        }

                    }
                };
                th.start();
            }
        });

    }

    public void PrintOut(ArrayList<CashModel> reslist){
        String result = "";
        for(CashModel res : reslist){
            result += res.toString();
            result += "\r\n";
        }
        textArea1.setText(result);
    }

}
